# Updating the system
sudo xbps-install -Su xbps -y
sudo xbps-install -Su -y
# Adding repos
sudo xbps-install void-repo-{nonfree,multilib,multilib-nonfree} -y
# Installing packages
sudo xbps-install -S gnome xdg-user-dirs xz dbus elogind xorg pipewire flatpak autoconf automake bison m4 make libtool flex meson ninja optipng sassc curl wget jq unzip wayland  -y
# Settings up xdg-user-dirs
xdg-user-dirs-update --force

# Installing wine dependencies
read -p "Do you want to install wine dependencies?(y/n): " user_input

case "$user_input" in
    "y")
        sudo xbps-install wine wine-32bit libpulseaudio-32bit freetype-32bit libgcc-32bit -y
        ;;
    "n")
        # command2 logic
        ;;
    *)
        echo "Invalid command"
        ;;
esac

# Installing drivers
read -p "Do you want to install FOSS drivers?(y/n): " user_input

case "$user_input" in
    "y")
        sudo xbps-install -S libgcc-32bit libstdc++-32bit libdrm-32bit libglvnd-32bit mesa-dri-32bit libdrm libdrm-32bit libglapi libglapi-32bit libva-glx libva-glx-32bit vulkan-loader mesa-vulkan-radeon mesa-vaapi mesa-vdpau amdvlk amdvlk-32bit -y
        ;;
    "n")
        # command2 logic
        ;;
    *)
        echo "Invalid command"
        ;;
esac

sh <(curl -L https://nixos.org/nix/install) --no-daemon
. /home/$USER/.nix-profile/etc/profile.d/nix.sh
echo ". /home/$USER/.nix-profile/etc/profile.d/nix.sh" > .bashrc
nix-env -iA nixpkgs.brave
nix-env -iA nixpkgs.gnomeExtensions.blur-my-shell
nix-env -iA nixpkgs.gnomeExtensions.appindicator
nix-env -iA nixpkgs.gnomeExtensions.gsconnect

# Starting services
sudo ln -s /etc/sv/dbus /var/service/
sudo ln -s /etc/sv/elogind /var/service/
sudo ln -s /etc/sv/NetworkManager /var/service/
sudo ln -s /etc/sv/pipewire /var/service/
sudo ln -s /etc/sv/pipewire-pulse /var/service/
sudo ln -s /etc/sv/gdm /var/service/