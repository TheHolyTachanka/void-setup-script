# Updating the system
sudo xbps-install -Su xbps -y
sudo xbps-install -Su -y
# Adding repos
sudo xbps-install void-repo-{nonfree,multilib,multilib-nonfree} -y
# Installing packages
sudo xbps-install dbus elogind xorg gnome pipewire flatpak autoconf automake bison m4 make libtool flex meson ninja optipng sassc curl wget jq unzip wayland  -y
# Installing nvidia drivers and libraries
read -p "Do you want to install nvidia drivers?(y/n): " user_input

case "$user_input" in
    "y")
        sudo xbps-install libdrm libdrm-32bit libglapi libglapi-32bit libva-glx libva-glx-32bit nvidia nvidia-gtklibs nvidia-gtklibs-32bit nvidia-libs-32bit -y
        ;;
    "n")
        # command2 logic
        ;;
    *)
        echo "Invalid command"
        ;;
esac


# Starting services
sudo ln -s /etc/sv/dbus /var/service/
sudo ln -s /etc/sv/elogind /var/service/
sudo ln -s /etc/sv/NetworkManager /var/service/
sudo ln -s /etc/sv/pipewire /var/service/
sudo ln -s /etc/sv/pipewire-pulse /var/service/
sudo ln -s /etc/sv/gdm /var/service/
# Adding flathub repo and installing some apps
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub com.brave.Browser
flatpak install flathub io.gdevs.GDLauncher
flatpak install flathub com.github.tchx84.Flatseal
flatpak install flathub com.discordapp.Discord